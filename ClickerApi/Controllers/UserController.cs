﻿using ClickerApi.Database;
using ClickerApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ClickerApi.Controllers
{
    public class UserController : ApiController
    {
        UserService userService;
        // GET api/valuess
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        public bool Get([FromUri] string user, [FromUri] string password)
        {
            userService = new UserService();
            return userService.Login(user, password);
        }

        // GET api/values/5
        public string Get(int id)
        {
            return "value";
        }

        public bool Post([FromBody] User user)
        {
            userService = new UserService();
            return userService.Register(user);
        }

        // PUT api/values/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
        }
    }
}
