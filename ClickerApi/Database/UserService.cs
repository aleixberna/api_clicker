﻿using ClickerApi.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace ClickerApi.Database
{
    public class UserService
    {
        private Connection cnx = new Connection();
        public bool Login(string user, string password)
        {
            string sql = $"SELECT * FROM USUARIO WHERE usuario='{user}' AND password='{password}'";
            cnx.Open();

            SqlDataReader dr = cnx.Select(sql);

            if (dr.HasRows)
            {
                cnx.Close();
                return true;
            }

            cnx.Close();
            return false;
        }

        public bool Register(User user)
        {
            User usuario = new User();
            cnx.Open();

            string sql_Select = $"SELECT * FROM USUARIO WHERE usuario='{user.Usuario}'";

            SqlDataReader drSegundo = cnx.Select(sql_Select);
            //Comprobar si el usuario existe

            if (drSegundo.HasRows)
            {
                cnx.Close();
                return false;
            }
            else
            {
                string sql = $"INSERT INTO USUARIO (nombre, apellido, email, usuario, password) VALUES('{user.Nombre}', '{user.Apellido}', '{user.Email}', '{user.Usuario}', '{user.Password}')";

                drSegundo.Close();
                cnx.ExecuteNonQuery(sql);

                cnx.Close();

            }

            cnx.Close();
            return true;

        }   
    }
}